#ifndef MY_DATA_TYPES
#define MY_DATA_TYPES
#include "../include/type.h"
#include <fstream>
#include <iostream>
#include <map>
#include <type_traits>
#include <unordered_map>
#include <vector>
using namespace std;

std::ostream &operator<<(std::ostream &strm, Type t);

double pos(double i, double j, int size);
template <typename T> void pprint(std::vector<T> V, int dim) {
  for (int i = 0; i < V.size() / dim; ++i) {
    for (int j = 0; j < dim; ++j) {
      std::cout << V[pos(i, j, dim)] << " ";
    }
    std::cout << std::endl;
  }
}
double count(vector<double> &V);
double count_to_set(vector<double> &V);
double eu_distance(double x1, double y1, double x2, double y2, double L);
double temperature(double x, double y);
double total_temperature(vector<double> &vx, vector<double> &vy,
                         vector<double> &vz);

double avg_vel(vector<double> &vx, vector<double>&vy, vector<double> &vz);
vector<double> dens_radial(vector<double> &X, int bins, int Lx);
vector<double> avg_vel_radial(vector<double> &X, vector<double> &vz, int bin,
                              int Lx);
vector<double> shear_radial(vector<double> &X, int bins, double Lx, int Ly, int Lz);
vector<double> temperature_radial(vector<double> &X, vector<double> &vx,
                                  vector<double> &vy, vector<double> &vz,
                                  int bins, int Lx, int Ly, int Lz);
double viscosity(vector<double> &X, vector<double> &vx,
                         vector<double> &vy, vector<double> &vz, double rho,
                         int Lx);
  vector<vector<int>> make_cell_list(vector<double> & X, vector<double> & Y,
                                     vector<double> & Z, double rc, double Lx,
                                     double Ly, double Lz);
  vector<int> nei_cells(int cell_index, double rc, int Lx, int Ly, int Lz);
  double omega(double r, double rc);
  vector<double> r_hat(double dis, vector<double> r);
  vector<double> Fc(double dis, vector<double> r, vector<double> rh, double a,
                    double rc);
  vector<double> Fd(double dis, vector<double> r, vector<double> rh,
                    vector<double> v, double rc);
  vector<double> Fr(double dis, vector<double> r, vector<double> rh, double rc);
  vector<double> Fs(double dis, vector<double> r, vector<double> rh, double Ks,
                    double rs);
  vector<double> Ft(double dis, vector<double> r, vector<double> v, double a,
                    double rc);
  double move(double x, double L);

  vector<double> linspace(double begin, double end, int n);
  map<int, vector<int>> init(
      vector<double> & X, vector<double> & Y, vector<double> & Z,
      vector<Type> & T, vector<double> & vx, vector<double> & vy,
      vector<double> & vz, vector<double> & fx, vector<double> & fy,
      vector<double> & fz, const double Lx, const double Ly, const double Lz,
      const double rho);

vector<double> integrate(vector<double> & X, vector<double> & Y, vector<double> & Z,
                         vector<Type> & T, vector<double> & vx, vector<double> & vy,
                         vector<double> & vz, vector<double> & fx, vector<double> & fy,
                         vector<double> & fz, double delta, double Lx, double Ly, double Lz,
                         vector<vector<int>> &cell_list, map<int, vector<int>> &chain_struct);

void update_forces(vector<double> & X, vector<double> & Y, vector<double> & Z,
                   vector<Type> & T, vector<double> & vx, vector<double> & vy,
                   vector<double> & vz, vector<double> & fx,
                   vector<double> & fy, vector<double> & fz, double cutoff,
                   double Lx, double Ly, double Lz, vector<vector<int>> &cl,
                   map<int, vector<int>> chain_struct, double dt);

#endif
