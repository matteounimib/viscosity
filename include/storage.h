#ifndef MY_STORAGE
#define MY_STORAGE
#include "../include/type.h"
#include <string>
#include <vector>
#include <map>
using namespace std;

void xyz(vector<double> &X, vector<double> &Y, vector<double> &Z,
         vector<Type> &T, const std::string filename);
void load_coord(vector<double> &X, vector<double> &Y, vector<double> &Z,
                vector<Type> &T, const string filename);
void load_vel(vector<double> &vx, vector<double> &vy, vector<double> &vz,
              const string filename);
void store_vel(vector<double> &vx, vector<double> &vy, vector<double> &vz,
               const string filename);
void store_molecules(map<int, vector<int>> &structure, string filename);
void load_molecules(map<int, vector<int>> &structure, string filename);
#endif
