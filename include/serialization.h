#ifndef MY_SERIAL
#define MY_SERIAL
#include <map>
#include <vector>
using namespace std;

vector<int> cl_to_vector(vector<vector<int>> &cl);
vector<vector<int>> vector_to_cl(vector<int> &cl_v);
vector<int> struct_to_vector(map<int, vector<int>> &st);
map<int, vector<int>> vector_to_struct(vector<int> &st_v);

vector<double> pack(vector<int> vj, vector<double> &fx,
                    vector<double> &fy, vector<double> &fz);

void unpack(vector<double> &message, vector<double> &fx, vector<double> &fy,
            vector<double> &fz);
#endif
