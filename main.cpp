#include "include/storage.h"
#include "include/structure.h"
#include "include/serialization.h"
#include <cmath>
#include <iostream>
#include <ostream>
#include <string>
#include <vector>
#include <mpi.h>
using namespace std;

int main(int argc, char *argv[]) {
  MPI_Init(NULL, NULL);
  srand(time(NULL));
  const double maxtime = 3;
  const double delta = 0.01;
  const double Lx = 12;
  const double Ly = 8;
  const double Lz = 8;
  const double rho = 6;
  double N = rho * Lx * Ly * Lz;
  const double rc = 1;
  double current_temp;
  // x,y,z: position
  vector<double> X = {};
  vector<double> Y = {};
  vector<double> Z = {};
  // t: type of particle 
  vector<Type> T = {};
  vector<int> T_conv = {};
  // velocities
  vector<double> vx = {};
  vector<double> vy = {};
  vector<double> vz = {};
  // forces
  vector<double> fx = {};
  vector<double> fy = {};
  vector<double> fz = {};
  vector<double> coord_vel= {};
  map<int, vector<int>> chain_struct;
  map<int, vector<int>> str_converted;
  vector<vector<int>> cell_list;
  vector<vector<int>> converted;
  vector<int> cell_list_conv;
  vector<int> struct_conv;
  int p = 0;
  int dim = 0;
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  if (rank == 0){
    chain_struct = init(X, Y, Z, T, vx, vy, vz, fx, fy, fz, Lx, Ly, Lz, rho);
    cell_list = make_cell_list(X, Y, Z, 1, Lx, Ly, Lz);
    coord_vel.insert(coord_vel.end(), X.begin(), X.end());
    coord_vel.insert(coord_vel.end(), Y.begin(), Y.end());
    coord_vel.insert(coord_vel.end(), Z.begin(), Z.end());
    coord_vel.insert(coord_vel.end(), vx.begin(), vx.end());
    coord_vel.insert(coord_vel.end(), vy.begin(), vy.end());
    coord_vel.insert(coord_vel.end(), vz.begin(), vz.end());
    MPI_Bcast(&coord_vel[0], N*6, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    for(Type i : T){
      T_conv.push_back(static_cast<int>(i));
    }
    MPI_Bcast(&T_conv[0], N, MPI_INT, 0, MPI_COMM_WORLD);
    cell_list_conv = cl_to_vector(cell_list);
    MPI_Bcast(&cell_list_conv[0], N + (Lx*Ly*Lz)/rc, MPI_INT, 0, MPI_COMM_WORLD);
    struct_conv = struct_to_vector(chain_struct);
    dim = struct_conv.size();
    str_converted = vector_to_struct(struct_conv);
    MPI_Bcast(&dim, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&struct_conv[0], dim, MPI_INT, 0, MPI_COMM_WORLD);
    // cout << "BEGIN\n";
    // vector<double> test = {0,0.2, 0.7,1 ,6,2,9};
    // vector<double> values = {1,1,1,1,1,1,1};
    // vector<double> out = avg_vel_radial(test, values, 10, values.size());
    // for (auto i : out){
    //   cout << i << endl;
    // }
  }else{
    X.resize(N);
    Y.resize(N);
    Z.resize(N);
    T_conv.resize(N);
    vx.resize(N);
    vy.resize(N);
    vz.resize(N);
    fx.resize(N);
    fy.resize(N);
    fz.resize(N);
    coord_vel.resize(N*6);
    cell_list_conv.resize(N + (Lx*Ly*Lz / rc));

    MPI_Bcast(&coord_vel[0], N * 6, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    X = {coord_vel.begin(), coord_vel.begin() + N};
    Y = {coord_vel.begin() + N, coord_vel.begin() + N * 2};
    Z = {coord_vel.begin() + N * 2 , coord_vel.begin() + N * 3};
    vx = {coord_vel.begin() + N * 3, coord_vel.begin() + N * 4};
    vy = {coord_vel.begin() + N * 4, coord_vel.begin() + N * 5};
    vz = {coord_vel.begin() + N * 5, coord_vel.begin() + N * 6};
    MPI_Bcast(&T_conv[0], N, MPI_INT, 0, MPI_COMM_WORLD);
    for (int i : T_conv){
      T.push_back(static_cast<Type>(i));
    }
    MPI_Bcast(&cell_list_conv[0], N+(Lx*Ly*Lz/rc), MPI_INT, 0, MPI_COMM_WORLD);
    cell_list = vector_to_cl(cell_list_conv);
    MPI_Bcast(&dim, 1, MPI_INT, 0, MPI_COMM_WORLD);
    struct_conv.resize(dim);

    MPI_Bcast(&struct_conv[0], dim, MPI_INT, 0, MPI_COMM_WORLD);
    chain_struct = vector_to_struct(struct_conv);
  }
  int bins = 200;
  int size = linspace(0, Lx, bins).size();
  vector<double> tmpAvg(size);
  vector<double> avg(size);
  double steps = maxtime * (1/delta);
  double volume = (static_cast<double>(Lx)/ static_cast<double>(bins))*
    static_cast<double>(Ly)*
    static_cast<double>(Lz);
  double visc = 0;
  for (double i = 0; i < maxtime; i += delta) {
    if (rank == 1)
      xyz(X, Y, Z, T, "test.xyz");
    integrate(X, Y, Z, T, vx, vy, vz, fx, fy, fz, delta, Lx, Ly, Lz,
              cell_list, chain_struct);
    cout << i << endl;
    cout << "temp " << total_temperature(vx,vy,vz) << endl;
    // tmpAvg = avg_vel_radial(X, vz, bins, Lx);
    // tmpAvg = dens_radial(X, bins, Lx);
    // tmpAvg = shear_radial(X, bins, 0.5 * Lx, Ly, Lz);
    tmpAvg = temperature_radial(X, vx, vy, vz, bins, Lx, Ly, Lz);
    // cout << "H\n";
    // vector<double> shear = shear_radial(X, bins, Lx, Ly, Lz);
    // tmpAvg = viscosity(X, vx, vy, vz, bins, Lx);
    visc += viscosity(X, vx, vy, vz, rho, Lx);
    for (int i = 0; i < tmpAvg.size(); ++i){
      avg[i] += tmpAvg[i] ;
    }
  }
  if (rank == 0){
    cout << "BEGIN OUT\n";
    for (int i = 0; i < avg.size(); ++i){
      // for density! Each bin should be normalized by the bin volume (fixed) and
      // by the number of steps
      // avg[i] = avg[i] / (volume * steps);
      // from the book (rdf)
      // avg[i] = avg[i] / (0.5 * N * steps * rho * volume);
      // for shear force. ?
      // avg[i] = avg[i] / (N * steps);
      // for temperature?
      avg[i] = avg[i] / (steps);
      // for velocities?
      // avg[i] = avg[i] / (0.5 * N * steps * volume);
      cout << avg[i] << endl;
    }
    visc = visc / (steps);
    cout << "viscosity: " << visc << endl;
  }
  // xyz(X, Y, Z, T, "final.xyz");
  MPI_Finalize();
  }
