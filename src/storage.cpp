//#include <vector>
#include "../include/structure.h"
#include "../include/serialization.h"
void xyz(vector<double> &X, vector<double> &Y, vector<double> &Z,
         vector<Type> &T, const std::string filename) {
  std::ofstream target;
  target.open(filename, std::ios_base::app);
  std::map<Type, std::string> mconv = {
      {Type::wall, "C"}, {Type::fluid, "H"}, {Type::a, "F"}, {Type::b, "O"}};
  target << X.size() << std::endl << std::endl;
  for (int i = 0; i < X.size(); ++i) {
    target << mconv.at(T[i]) << " " << X[i] << " " << Y[i] << " " << Z[i]
           << std::endl;
  }
  target.close();
}

// load xyz initial conditions from saved file
void load_coord(vector<double> &X, vector<double> &Y, vector<double> &Z,
          vector<Type> &T, const string filename){
  static unordered_map<std::string, Type> const table =
    {{"C", Type::wall},
     {"H", Type::fluid},
     {"F", Type::a},
     {"O", Type::b}};

  enum Type { wall = 0, fluid = 1, a = 2, b = 3 };
  ifstream data(filename);
  string tmp;
  double n;
  // skip first two lines
  getline(data, tmp);
  getline(data, tmp);
  cout << tmp << endl;
  string types;
  Type type;
  double x, y, z;
  while(data >> types >> x >> y >> z){
    auto it = table.find(types);
    T.push_back(it->second);
    X.push_back(x);
    Y.push_back(y);
    Z.push_back(z);
  }
}
void load_vel(vector<double> &vx, vector<double> &vy, vector<double> &vz,
              const string filename){
  ifstream data(filename);
  double x, y, z;
  while (data >> x >> y >> z) {
    vx.push_back(x);
    vy.push_back(y);
    vz.push_back(z);
  }
  }
void store_vel(vector<double> &vx, vector<double> &vy, vector<double> &vz,
               const string filename) {
  ofstream writer;
  writer.open(filename);
  for (int i = 0; i < vx.size(); ++i){
    writer << vx[i] << " " << vy[i] << " " << vz[i] << endl;
  }
  writer.close();
}

void store_molecules(map<int, vector<int>> &structure, string filename){
  vector<int> converted = struct_to_vector(structure);
  ofstream writer;
  writer.open(filename);
  for (int i : converted){
    writer << i << endl;
  }
  writer.close();
}

void load_molecules(map<int, vector<int>> &structure, string filename){
  ifstream data(filename);
  vector<int> tmp;
  int x = 0;
  while (data >> x){
    tmp.push_back(x);
  }
  structure = vector_to_struct(tmp);
}
