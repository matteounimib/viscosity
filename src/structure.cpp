#include "../include/structure.h"
#include "../include/serialization.h"
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <iterator>
#include <map>
#include <math.h>
#include <numeric>
#include <ostream>
#include <random>
#include <string>
#include <type_traits>
#include <utility>
#include <vector>
#include <unordered_map>
#include <mpi.h>
#define g 0.055
using namespace std;

map<vector<Type>, double> interactions = {
    {{Type::a, Type::a}, 50},         {{Type::a, Type::b}, 25},
    {{Type::a, Type::fluid}, 25},     {{Type::a, Type::wall}, 200},

    {{Type::b, Type::a}, 25},         {{Type::b, Type::b}, 1},
    {{Type::b, Type::fluid}, 300},    {{Type::b, Type::wall}, 200},

    {{Type::fluid, Type::a}, 25},     {{Type::fluid, Type::b}, 300},
    {{Type::fluid, Type::fluid}, 25}, {{Type::fluid, Type::wall}, 200},

    {{Type::wall, Type::a}, 200},     {{Type::wall, Type::b}, 200},
    {{Type::wall, Type::fluid}, 200}, {{Type::wall, Type::wall}, 0}};
std::ostream &operator<<(std::ostream &strm, Type t) {
  const std::string name[] = {"w", "f", "a", "b"};
  return strm << name[t];
}

double count(vector<double> &V) {
  double result = 0;

  for (int i = 0; i < V.size(); ++i) {
    result += V[i];
  }
  return result;
}

double omega(double r, double rc) {
  if (r <= rc)
    return 1 - r / rc;
  else
    return 0;
}
vector<double> r_hat(double dis, vector<double> r) {
  vector<double> result = {};
  for (int i = 0; i < r.size(); ++i) {
    result.push_back(r[i] / dis);
  }
  return result;
}

vector<double> Fc(double dis, vector<double> r, vector<double> rh, double a,
                  double rc) {
  vector<double> result = {};
  if (dis >= rc) {
    return {0, 0, 0};
  } else {
    for (int i = 0; i < rh.size(); ++i) {
      result.push_back(a * (1 - dis / rc) * rh[i]);
    }
    return result;
  }
}

vector<double> Fd(double dis, vector<double> r, vector<double> rh,
                  vector<double> v, double rc) {
  // double gamma = 4.5;
  double gamma = 20.25;
  vector<double> result = {};
  double w = pow(omega(dis, rc), 2);
  double prod = inner_product(rh.begin(), rh.end(), v.begin(), 0.0);
  for (int i = 0; i < r.size(); ++i) {
    result.push_back(-gamma * w * dis * prod * rh[i]);
  }
  return result;
}

vector<double> Fr(double dis, vector<double> r, vector<double> rh, double rc, double dt) {
  std::random_device rd{};
  std::mt19937 gen{rd()};
  std::normal_distribution<double> norm{0, 1};
  const double sigma = 4.5;
  vector<double> result = {};
  double w = omega(dis, rc);
  // cout << "w " << w << endl;
  double xi = norm(gen);
  for (int i = 0; i < r.size(); ++i) {
    result.push_back(sigma * w * dis * xi * rh[i] / sqrt(dt));
    // result.push_back(sigma * w * xi * rh[i] / sqrt(dt));
    // result.push_back(sigma * w * xi * rh[i]);
  }
  return result;
}

// total force
vector<double> Ft(double dis, vector<double> r, vector<double> v, double a,
                  double rc, double dt) {

  vector<double> rh = r_hat(dis, r);
  // vector<double> fc = Fc(dis, r, rh, a, rc);
  vector<double> fd = Fd(dis, r, rh, v, rc);
  vector<double> fr = Fr(dis, r, rh, rc, dt);
  vector<double> result = {};
  for (int i = 0; i < fd.size(); ++i) {
    // result.push_back(fc[i] + fd[i] + fr[i]);
    result.push_back(fd[i] + fr[i]);
  }
  // testing that is a unit vector
  // cout << sqrt(rh[0] * rh[0] + rh[1] * rh[1] + rh[2] * rh[2])<< endl;
  return result;
}
// bond force
vector<double> Fs(double dis, vector<double> r, vector<double> rh, double Ks,
                  double rs) {
  vector<double> result = {};
  for (int i = 0; i < rh.size(); ++i) {
    result.push_back(Ks * (1 - dis / rs) * rh[i]);
  }
  // cout << sqrt(rh[0]*rh[0] + rh[1]*rh[1] + rh[2] * rh[2]) << endl;
  return result;
}

vector<vector<int>> make_cell_list(vector<double> &X, vector<double> &Y,
                                   vector<double> &Z, double rc,
                                   double Lx, double Ly, double Lz) {
  vector<vector<int>> result;
  double xmin = -Lx / 2;
  double ymin = -Ly / 2;
  double zmin = -Lz / 2;
  int posx = 0;
  int posy = 0;
  int posz = 0;
  int pos = 0;
  // initialize the data structure adding the cell IDs

  for (int i = 0; i < Lx * Ly * Lz; i += rc) {
    result.push_back({});
  }
  // add the parciles
  for (int i = 0; i < X.size(); ++i) {
    posx = floor((X[i] - xmin) / rc);
    posy = floor((Y[i] - ymin) / rc);
    posz = floor((Z[i] - zmin) / rc);
    // cout << i << endl;
    pos = (posx + posy * Lx + posz * Lx * Ly);
    // cout << "posx " << posx << endl << "posy " << posy << endl
    //      << "posz " << posz << endl << "POS " << pos << "size "
    //      << result.size() << endl;
    result[pos].push_back(i);
  }
  return result;
}
void update_cell(vector<vector<int>> &cl, int i, double old_x,
                 double old_y, double old_z, double new_x, double new_y,
                 double new_z, double rc, double xmin, double ymin,
                 double zmin, double Lx, double Ly, double Lz) {
  int posx = floor((old_x - xmin) / rc);
  int posy = floor((old_y - ymin) / rc);
  int posz = floor((old_z - zmin) / rc);
  int cell_index = (posx + posy * Lx + posz * Lx * Ly);
  cl[cell_index].erase(
      std::remove(cl[cell_index].begin(), cl[cell_index].end(), i),
      cl[cell_index].end());
  posx = floor((new_x - xmin) / rc);
  posy = floor((new_y - ymin) / rc);
  posz = floor((new_z - zmin) / rc);
  // cell_index = (posz + posy * Lx + posx * Lx * Ly);
  cell_index = (posx + posy * Lx + posz * Lx * Ly);
  cl[cell_index].push_back(i);
}

// return neighbour cells given the cell index
vector<int> nei_cells(int cell_index, double rc, int Lx, int Ly, int Lz) {
  vector<int> result = {};
  // // get the adj cells
  // // get i j coordinates of the given cell
  int cell_z = cell_index / (Lx * Ly);
  int cell_y = (cell_index % (Lx * Ly)) / Lx;
  int cell_x = (cell_index % (Lx * Ly)) % Lx;
  // cell_z = cell_index / (Lx * Ly);
  // cout << "coord x: " << cell_x << endl;
  // cout << "coord y: " << cell_y << endl;
  // cout << "coord z: " << cell_z << endl;
  int tmpx = 0;
  int tmpy = 0;
  int tmpz = 0;
  int target = 0;
  int tot = 0;
  for (int i = cell_z - 1; i <= cell_z + 1; ++i) {
    tmpz = i;
    if (tmpz == -1)
      tmpz = Lz - 1;
    if (tmpz == Lz) {
      tmpz = 0;
    }
    for (int j = cell_y - 1; j <= cell_y + 1; ++j) {
      tmpy = j;
      if (tmpy == -1)
        tmpy = Ly -1;
      if (tmpy == Ly)
        tmpy = 0;
      for (int e = cell_x - 1; e <= cell_x + 1; ++e) {
        tmpx = e;
        if (tmpx == -1)
          tmpx = Lx -1;
        if (tmpx == Lx)
          tmpx = 0;
        target = tmpx + tmpy * Lx + tmpz * Lx * Ly;
        tot++;
        if (!(find(result.begin(), result.end(), target) != result.end()) &&
            target != cell_index) {
          result.push_back(target);
          }
      }
    }
  }

  return result;
}

// return particles given an index cell
vector<int> getParticles(vector<vector<int>> &cl, int cell_index) {
  return cl[cell_index];
}

// return particles ids given an index cell
vector<int> getParticlesCell(vector<vector<int>> &cl, int cell_index) {
  vector<int> result = {};
  if (cl[cell_index].size() > 0) {
    for (int i = 0; i < cl[cell_index].size(); ++i) {
      result.push_back(cl[cell_index][i]);
    }
  }
  return result;
}

// return particles ids of the given cell and of their neighbours
vector<int> getParticlesNei(vector<vector<int>> &cl, int cell_index,
                            int rc, double Lx, double Ly, double Lz) {
  vector<int> result = {};
  vector<int> nei = nei_cells(cell_index, rc, Lx, Ly, Lz);
  result = getParticlesCell(cl, cell_index);
  for (int i : nei){
    for (int j : cl[i]){
      result.push_back(j);
    }
  }
  return result;
}

double eu_distance(double x1, double y1, double x2, double y2, double z1,
                   double z2, double Lx, double Ly, double Lz) {
  double xr = move(x2 - x1, Lx);
  double yr = move(y2 - y1, Ly);
  double zr = move(z2 - z1, Lz);
  return sqrt(xr * xr + yr * yr + zr * zr);
}

// temperature of the system where each particle has mass = 1 (and K_b = 1)
double total_temperature(vector<double> &vx, vector<double> &vy,
                         vector<double> &vz) {
  double result = 0;
  for (int i = 0; i < vx.size(); ++i) {
    result +=  (vx[i] * vx[i] + vy[i] * vy[i]);
    // result += (vx[i] * vx[i] + vy[i] * vy[i] + vz[i] * vz[i]);
  }
  return result / (vx.size() * 2);
}
double avg_vel(vector<double> &vx, vector<double>&vy, vector<double> &vz){
  double result = 0;
  for(int i = 0; i < vx.size(); ++i){
    // result += sqrt(vx[i] * vx[i] + vy[i] * vy[i] + vz[i] * vz[i]);
    result += sqrt(vz[i] * vz[i]);
  }
  return result / vx.size();
}
vector<double> dens_radial(vector<double> &X, int bins, int Lx) {
  double xmin = -Lx / 2;
  vector<double> sup = linspace(0, Lx, bins);
  int size = sup.size();
  vector<double> result(size);
  vector<double>::iterator bin;
  int index = 0;
  for (int i = 0; i < X.size(); ++i) {
    bin = upper_bound(sup.begin(), sup.end(), X[i] - xmin);
    index = bin - sup.begin() - 1;
    result[index] += 1.0;
  }
  return result;
}
vector<double> avg_vel_radial(vector<double> &X, vector<double> &vz,
                        int bins, int Lx){
  double xmin = -Lx/2;
  vector<double> sup =  linspace(0, Lx, bins);
  int size = sup.size();
  vector<double> result(size);
  vector<double> count(size);
  vector<double>::iterator bin;
  int index = 0;
  int mean = 0;
  // cout << "BINS\n";
  // for (auto i : sup)
  //   cout << i << endl;
  // cout << "end bins\n";
  for (int i = 0; i < X.size(); ++i){
    bin = upper_bound(sup.begin(), sup.end(), X[i] - xmin);
    index = bin - sup.begin() - 1;
    result[index] += vz[i];
    mean += vz[i];
    count[index] ++;
  }
  mean = mean / X.size();
  for (int i = 0; i < result.size(); ++i){
    result[i] = result[i] / count[i];
    // cout << "res " << result[i] << " " << count[i] << " " << i << endl;
    }
  return result;
}

vector<double> shear_radial(vector<double> &X, int bins, double Lx, int Ly, int Lz)
{
  double xmin = -Lx;
  vector<double> sup = linspace(0, static_cast<int>(Lx), bins);
  int size = sup.size();
  vector<double> result(size);
  vector<double> count(size);
  vector<double>::iterator bin;
  int index = 0;
  for (int i = 0; i < X.size(); ++i) {
    bin = upper_bound(sup.begin(), sup.end(), X[i] - xmin);
    index = bin - sup.begin() - 1;
    count[index] += i;

  }
  double volume = (static_cast<double>(Lx)/ static_cast<double>(bins))*
    static_cast<double>(Ly)*
    static_cast<double>(Lz);
  double tmpg = -g;
  for (int i = 0; i < result.size(); ++i){
    // cout << i << endl;
    result[i] = (count[i] / volume) * tmpg * (sup[i] - 0.5 * static_cast<double>(Lx));
    // cout << "sup " << result[i] << endl;
    // cout << "sup " << g * (sup[i] - 0.5 * Lx) << endl;
    // g should be opposite for the bins placed on the 
    // if (i == result.size() / 2)
    //   tmpg = g;
    // cout << result[i] << endl;
}
  return result;
}
vector<double> temperature_radial(vector<double> &X, vector<double> &vx,
                                  vector<double> &vy, vector<double> &vz,
                                  int bins, int Lx, int Ly, int Lz){
  double xmin = -Lx / 2;
  vector<double> sup = linspace(0, Lx, bins);
  int size = sup.size();
  vector<double> result(size);
  vector<double> count(size);
  vector<vector<double>>container(size);
  vector<double>::iterator bin;
  int index = 0;
  vector<double> vxp = {};
  vector<double> vyp = {};
  vector<double> vzp = {};
  for (int i = 0; i < X.size(); ++i) {
    bin = upper_bound(sup.begin(), sup.end(), X[i] - xmin);
    index = bin - sup.begin() -1;
    count[index] += i;
    // cout << i << endl;
    container[index].push_back(i);
  }
  for (int i = 0; i < result.size(); ++i) {
    vxp = {};
    vyp = {};
    vzp = {};
    for (double index : container[i]){
      vxp.push_back(vx[index]);
      vyp.push_back(vy[index]);
      vzp.push_back(vz[index]);
    }
    if (vx.empty())
      result [i] = 0;
    else
      result[i] = total_temperature(vxp, vyp, vzp);
  }
  return result;
}

double viscosity(vector<double> &X, vector<double>&vx,
                         vector<double>&vy, vector<double> &vz,
                         double rho, int Lx){
  // TODO: do again in total system, not bins. <Vz> is the average of all system velocities
  double xmin = -Lx / 2;
  double result = 0;
  double mean = 0;
  double count = 0.0;
  // vector<double> sup = linspace(0, Lx, bins);
  // int size = sup.size();
  // vector<double>::iterator bin;
  // vector<double> result(size);
  // vector<double> count(size);
  // vector<double> densities = dens_radial(X, bins, Lx);
  // vector<double> velocities = avg_vel_radial(X, vz, bins, Lx);
  // for (int i = 0; i < result.size(); ++i){
  // result[i] = (densities[i] * g * Lx * Lx)/(12 * velocities[i]);
  // }
  for (int i = 0; i < X.size(); ++i){
    if (X[i] - xmin < Lx * 0.5){
      mean += sqrt(vx[i] * vx[i] + vy[i] * vy[i] + vz[i] * vz[i]);
      count++;
    }
  }
  mean = mean / count;
  result = rho * g * pow((0.5 * Lx), 2) / (12 * mean) ;
  return result;
}


map<int, vector<int>> chain(int n, vector<double> &X, vector<double> &Y,
                            vector<double> &Z, vector<Type> &T,
                            vector<double> &vx, vector<double> &vy,
                            vector<double> &vz, vector<double> &fx,
                            vector<double> &fy, vector<double> &fz,
                            const double Lx, const double Ly,
                            const double Lz) {
  std::random_device rd{};
  std::mt19937 gen{rd()};
  std::uniform_real_distribution<> distx(-Lx / 2, Lx / 2);
  std::uniform_real_distribution<> disty(-Ly / 2, Ly / 2);
  std::uniform_real_distribution<> distz(-Lz / 2, Lz / 2);
  map<int, vector<int>> result;
  // set position of chain head randomly
  double hx = 0;
  double hy = 0;
  double hz = 0;
  int pos = 0;
  for (int i = 0; i < n; ++i) {
    hx = distx(gen);
    hy = disty(gen);
    hz = distz(gen);
    result.insert(std::pair<int, vector<int>>(i, {}));
    for (int j = 0; j < 7; ++j) {
      X.push_back(move((hx + j * 0.05), Lx));
      Y.push_back(move((hy + j * 0.05), Ly));
      Z.push_back(move((hz + j * 0.05), Lz));
      vx.push_back(0);
      vy.push_back(0);
      vz.push_back(0);
      fx.push_back(0);
      fy.push_back(0);
      fz.push_back(0);
      if (j < 2) {
        T.push_back(Type::a);
      } else {
        T.push_back(Type::b);
      }
      result.find(i)->second.push_back(pos);
      pos++;
    }
  }
  return result;
}

vector<double> linspace(double begin, double end, int n) {
  vector<double> result = {};
  double ratio = (end - begin) / n;
  double i = begin;
  while (i < end) {
    result.push_back(i);
    i += ratio;
  }
  return result;
}

//  3d completed using rings on the same plane
map<int, vector<int>> ring(int n, vector<double> &X, vector<double> &Y,
                           vector<double> &Z, vector<Type> &T,
                           vector<double> &vx, vector<double> &vy,
                           vector<double> &vz, vector<double> &fx,
                           vector<double> &fy, vector<double> &fz,
                           const double Lx, const double Ly,
                           const double Lz) {

  std::random_device rd{};
  std::mt19937 gen{rd()};
  std::uniform_real_distribution<> distx(-Lx / 2 + 1.4, Lx / 2 - 1.4);
  std::uniform_real_distribution<> disty(-Ly / 2 + 1.4, Ly / 2 - 1.4);
  std::uniform_real_distribution<> distz(-Lz / 2 + 1.4, Lz / 2 - 1.4);
  map<int, vector<int>> result;
  double radius = 0.5;
  double centerx = 0.0;
  double centery = 0.0;
  double centerz = 0.0;
  vector<double> points = linspace(0, 2 * M_PI, 9);
  // set position of chain head randomly
  int pos = 0;
  for (int i = 0; i < n; ++i) {
    centerx = distx(gen);
    centery = disty(gen);
    centerz = distz(gen);
    result.insert(std::pair<int, vector<int>>(i, {}));
    for (auto j : points) {
      X.push_back(move((centerx + radius * cos(j)), Lx));
      Y.push_back(move((centery + radius * sin(j)), Ly));
      Z.push_back(move((centerz + radius), Lz));
      vx.push_back(0);
      vy.push_back(0);
      vz.push_back(0);
      fx.push_back(0);
      fy.push_back(0);
      fz.push_back(0);
      T.push_back(Type::a);
      result.find(i)->second.push_back(pos);
      pos++;
    }
  }
  return result;
}
map<int, vector<int>> init(vector<double> &X, vector<double> &Y,
                           vector<double> &Z, vector<Type> &T,
                           vector<double> &vx, vector<double> &vy,
                           vector<double> &vz, vector<double> &fx,
                           vector<double> &fy, vector<double> &fz,
                           const double Lx, const double Ly,
                           const double Lz, const double rho) {
  std::random_device rd{};
  std::mt19937 gen{rd()};
  std::uniform_real_distribution<> distx(-Lx / 2, Lx / 2);
  std::uniform_real_distribution<> disty(-Ly / 2, Ly / 2);
  std::uniform_real_distribution<> distz(-Lz / 2, Lz / 2);
  const double wall_size = 1;
  const int chains = 0;
  const int ring_len = 7;
  double N = rho * Lx * Ly * Lz;
  map<int, vector<int>> chain_struct =
    chain(chains, X, Y, Z, T, vx, vy, vz, fx, fy, fz, Lx, Ly, Lz);
  for (int i = ring_len * chains; i < N; ++i) {
    double x_pos = distx(gen);
    X.push_back(x_pos);
    Y.push_back(disty(gen));
    Z.push_back(distz(gen));
    T.push_back(Type::fluid);

    vy.push_back(0);
    vx.push_back(0);
    vz.push_back(0);

    fx.push_back(0);
    fy.push_back(0);
    fz.push_back(0);
  }
  return chain_struct;
}

// move in periodic boundary conditions
double move(double x, double L) { return x - L * round(x / L); }

// split vector in n subvectors to use mpi
vector<vector<double>> split(vector<double> &V, int n){
  vector<vector<double>> result = {};
  int v_dim = V.size() / n;
  for (int i = 0; i < n; ++i){
    result.push_back({});
    for (int j = 0; j < v_dim; ++j){
      result[i].push_back(V[j + i * v_dim]);
    }
  }
  return result;
}
vector<double> integrate(vector<double> &X, vector<double> &Y,
                         vector<double> &Z, vector<Type> &T, vector<double> &vx,
                         vector<double> &vy, vector<double> &vz,
                         vector<double> &fx, vector<double> &fy,
                         vector<double> &fz, double delta, double Lx,
                         double Ly, double Lz,
                         vector<vector<int>> &cell_list,
                         map<int, vector<int>> &chain_struct) {
  double old_x;
  double old_y;
  double old_z;
  vector<double> fxp = {};
  vector<double> fyp = {};
  vector<double> fzp = {};
  vector<double> vxp = vx;
  vector<double> vyp = vy;
  vector<double> vzp = vz;
  vector<double> result = {};
  for (int i = 0; i < X.size(); ++i) {
    old_x = X[i];
    old_y = Y[i];
    old_z = Z[i];
    if (T[i] != Type::wall) {
      X[i] = move(X[i] + vx[i] * delta + 0.5 * fx[i] * delta * delta, Lx);
      Y[i] = move(Y[i] + vy[i] * delta + 0.5 * fy[i] * delta * delta, Ly);
      Z[i] = move(Z[i] + vz[i] * delta + 0.5 * fz[i] * delta * delta, Lz);
    } else {
      Y[i] = move(Y[i] + vy[i] * delta, Ly);
    }
    update_cell(cell_list, i, old_x, old_y, old_z, X[i], Y[i], Z[i], 1,
                -Lx / 2, -Ly / 2, -Lz / 2, Lx, Ly, Lz);
    // vxp = vx;
    // vyp = vy;
    // vzp = vz;
    if (T[i] != Type::wall) {
      // with lambda = 0.5
      vxp[i] = vx[i] + 0.5 * fx[i] * delta;
      vyp[i] = vy[i] + 0.5 * fy[i] * delta;
      vzp[i] = vz[i] + 0.5 * fz[i] * delta;
    }
  }
  fxp = fx;
  fyp = fy;
  fzp = fz;
  // cout << "size " << cell_list.size() << endl;
  // cl_container = split_cl(cell_list, 2, L);
  update_forces(X, Y, Z, T, vxp, vyp, vzp, fx, fy, fz, 1, Lx, Ly,
                Lz, cell_list, chain_struct, delta);
  for (int i = 0; i < X.size(); ++i) {
    if (T[i] != Type::wall) {
      vx[i] += 0.5 * delta * (fx[i] + fxp[i]);
      vy[i] += 0.5 * delta * (fy[i] + fyp[i]);
      vz[i] += 0.5 * delta * (fz[i] + fzp[i]);
    }
  }
  // cout << "temperature " << total_temperature(vx, vy) << endl;
  // result.push_back(total_temperature(vx, vy));
  // cout  << "vel x: " << count(vx)  << endl;
  // cout << "vel y: " << count(vy) << endl << endl;
  return result;
}

void update_forces(vector<double> &X, vector<double> &Y, vector<double> &Z,
                   vector<Type> &T, vector<double> &vx, vector<double> &vy,
                   vector<double> &vz, vector<double> &fx, vector<double> &fy,
                   vector<double> &fz, double cutoff, double Lx,
                   double Ly, double Lz,
                   vector<vector<int>> &cl,
                   map<int, vector<int>> chain_struct, double dt) {
  const double xmin = -Lx / 2;
  double posx = 0;
  double r2x = 0.0;
  double r2y = 0.0;
  double r2z = 0.0;
  double xr = 0.0;
  double yr = 0.0;
  double zr = 0.0;
  vector<int> nei = {};
  vector<double> tmp = {0, 0, 0};
  double c2 = cutoff * cutoff;
  double actf = 0;
  vector<double> rhc = {};
  int tot = 2;
  map<int, vector<int>>::iterator it;
  for (int i = 0; i < X.size(); ++i) {
    fx[i] = 0.0;
    fy[i] = 0.0;
    fz[i] = 0.0;
  }
  vector<double> localFx = fx;
  vector<double> localFy = fy;
  vector<double> localFz = fz;
  // without cell list
  // for (int i = 0; i < X.size() -1; ++i) {
  //     // nei = neighbour(cl, i, X, Y, cutoff, L);
  //   for (int j = i + 1; j < X.size(); ++j) {
  // with cell list, fast implementation
  // begin
  int cores;
  MPI_Comm_size(MPI_COMM_WORLD, &cores);
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int other = 0;
  vector<int> particles = {};
  vector<int> particlesCell = {};
  int dim = 0;
  for (int c = 0; c < cl.size(); ++c) {
    if (c % cores == rank) {
      particlesCell = getParticlesCell(cl, c);
      particles = getParticlesNei(cl, c, cutoff, Lx, Ly, Lz);
      tot++;
      // if (particles.size() == 0)
      //   continue;
      for (int i : particlesCell) {
        for (int j : particles) {
          // if (X[i] == X[j] && Y[i] == Y[j] && Z[i] == Z[j])
          //   continue;
          if (i > j) {
            posx = X[i] - xmin;
            xr = move(X[i] - X[j], Lx);
            yr = move(Y[i] - Y[j], Ly);
            zr = move(Z[i] - Z[j], Lz);
            r2x = xr * xr;
            r2y = yr * yr;
            r2z = zr * zr;
            r2x = r2x + r2y + r2z;
            if (r2x < c2) {
              r2x = sqrt(r2x);
              actf = interactions.find({T[i], T[j]})->second;

              // tmp = Ft(r2x, {xr, yr, zr},
              //          {vx[i] - vx[j], vy[i] - vy[j], vz[i] - vz[j]}, actf,
              //          cutoff, dt);
              tmp = Ft(r2x, {xr, yr, zr},
                       {vx[i] - vx[j],
                        vy[i] - vy[j],
                        vz[i] - vz[j]}, actf,
                       cutoff, dt);
              if (posx  <= Lx/2){
                localFx[i] += tmp[0];
                localFy[i] += tmp[1];
                localFz[i] += tmp[2] - g;
              }else{
                localFx[i] += tmp[0];
                localFy[i] += tmp[1];
                localFz[i] += tmp[2] + g;
              }

              localFx[j] += -tmp[0];
              localFy[j] += -tmp[1];
              localFz[j] += -tmp[2];
            }
          }
        }
        // cout << "tot: " << tot << endl;
      }
    }
  }
  MPI_Allreduce(localFx.data(), fx.data(), fx.size(), MPI_DOUBLE, MPI_SUM,
                MPI_COMM_WORLD);
  MPI_Allreduce(localFy.data(), fy.data(), fy.size(), MPI_DOUBLE, MPI_SUM,
                MPI_COMM_WORLD);
  MPI_Allreduce(localFz.data(), fz.data(), fz.size(), MPI_DOUBLE, MPI_SUM,
                MPI_COMM_WORLD);
}
