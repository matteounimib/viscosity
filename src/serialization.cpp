#include "../include/serialization.h"
#include <iostream>

using namespace std;

// serialize the cell list
// {{1,2,3},{0,5,9},{4,7}} -> {-1, 1, 2, 3, -1, 0, 5, 9, -1, 4, 7}
vector<int> cl_to_vector(vector<vector<int>> &cl) {
  vector<int> result = {};
  for (vector<int> v : cl){
    result.push_back(-1);
    for (int i : v){
      result.push_back(i);
    }
  }
  return result;
}

// build cell list from serialized vector
vector<vector<int>> vector_to_cl(vector<int> &cl_v){
  vector<vector<int>> result = {};
  for (int i : cl_v){
    if (i == -1){
      result.push_back({});
    }else{
      result.back().push_back(i);
    }
  }
  return result;
}

// serialize molecules struct from map to vector
vector<int> struct_to_vector(map<int, vector<int>> &st){
  vector<int> result = {};
  map<int, vector<int>>::iterator it;
  for (it = st.begin(); it != st.end(); it++){
    result.push_back(-1);
    for(int i : it->second){
      result.push_back(i);
    }
  }
  return result;
}
// build molecules structure map from vector
map<int, vector<int>> vector_to_struct(vector<int> &st_v){
  map<int, vector<int>> result = {};
  vector<int> tmp = {};
  map<int, vector<int>>::iterator it;
  int index = -1;
  for (int i : st_v){
    if (i == -1){
      index++;
      result.insert(result.end(), pair<int, vector<int>>(index, {}));
    }else{
      it = result.find(index);
      it->second.push_back(i);
    }
  }
  return result;
}

// vector<vector<double>> pack(vector<double> i, vector<double> j,
//                             vector<double> fxi, vector<double> fyi,
//                             vector<double> fzi, vector<double> fxj,
//                             vector<double> fyj, vector<double> fzj) {

//   vector<vector<double>> result = {};
//   result.push_back(i);
//   result.push_back(j);
//   result.push_back(fxi);
//   result.push_back(fyi);
//   result.push_back(fzi);
//   result.push_back(fxj);
//   result.push_back(fyj);
//   result.push_back(fzj);
//   return result;
// }

// void unpack(vector<vector<double>> &message, vector<double> &fx,
//             vector<double> &fy, vector<double> &fz) {
//   for (int c = 0; c < message[0].size(); ++c) {
//     int i = (int)message[0][c];
//     int j = (int)message[1][c];

//     fx[i] = message[2][c];
//     fy[i] = message[3][c];
//     fz[i] = message[4][c];

//     fx[j] = message[5][c];
//     fy[j] = message[6][c];
//     fz[j] = message[7][c];
//   }
// }
// vector<double> pack(vector<double> i, vector<double> j, vector<double> fxi,
//                             vector<double> fyi, vector<double> fzi,
//                             vector<double> fxj, vector<double> fyj, vector<double> fzj){

//   vector<double> result ={};
//   result.insert(result.end(), i.begin(), i.end());
//   result.insert(result.end(), j.begin(), j.end());
//   result.insert(result.end(), fxi.begin(), fxi.end());
//   result.insert(result.end(), fyi.begin(), fyi.end());
//   result.insert(result.end(), fzi.begin(), fzi.end());
//   result.insert(result.end(), fxj.begin(), fxj.end());
//   result.insert(result.end(), fyj.begin(), fyj.end());
//   result.insert(result.end(), fzj.begin(), fzj.end());
//   return result;
// }
// void unpack(vector<double> &message, vector<double> &fx, vector<double> &fy,
//             vector<double> &fz) {
//   for (int c = 0; c < message.size() / 8; c += 8) {
//     int i = (int)message[c];
//     int j = (int)message[c + 1];
//     fx[i] = message[c + 2];
//     fy[i] = message[c + 3];
//     // cout << "my j " << message[c+1] << endl;
//     fz[i] = message[c + 4];

//     fx[j] = message[c + 5];
//     fy[j] = message[c + 6];
//     fz[j] = message[c + 7];
//   }
// }
vector<double> pack(vector<int> vj,
                    vector<double> &fx, vector<double> &fy, vector<double> &fz){

  vector<double> result = {};
    for (int j : vj) {
      result.push_back(j * 1.0);
      result.push_back(fx[j]);
      result.push_back(fy[j]);
      result.push_back(fz[j]);
    }
  // }
  return result;
}
void unpack(vector<double> &message, vector<double> &fx, vector<double> &fy,
            vector<double> &fz) {
  for (int c = 0; c < message.size(); c += 4) {
    int index = (int)message[c];
    fx[index] = message[c + 1];
    fy[index] = message[c + 2];
    fz[index] = message[c + 3];

  }
}
