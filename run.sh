#!/bin/bash
#SBATCH --time=00:10:00
#SBATCH --output=molecules-%j.out
#SBATCH --error=errors-%j.err
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=20
#SBATCH -J MPI_jobMAT

time mpirun ./model
echo running...
