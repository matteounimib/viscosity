CC = mpic++
CFLAGS = -Ofast 
model: main.o structure.o serialization.o storage.o 
	$(CC) $(CFLAGS) main.o structure.o serialization.o storage.o -o model

main.o: main.cpp
	$(CC) $(CFLAGS) -g -c main.cpp

structure.o: src/structure.cpp include/structure.h
	$(CC) $(CFLAGS) -g -c src/structure.cpp

serialization.o: src/serialization.cpp include/serialization.h
	$(CC) $(CFLAGS) -g -c src/serialization.cpp

storage.o: src/storage.cpp include/storage.h
	$(CC) $(CFLAGS) -g -c src/storage.cpp

#type.o: include/type.h
#	$(CC) $(CFLAGS) -g -c include/type.h
clean:
	rm *.o
